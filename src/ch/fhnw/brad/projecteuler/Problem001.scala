package ch.fhnw.brad.projecteuler
import scala.annotation.tailrec
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem001 {

  def main(args: Array[String]): Unit = {
    var sum = 0
    for(i <- 2 to 999) {
      if (i % 3 == 0 || i % 5 == 0) sum += i
    }
    println(sum)
  }
  

}