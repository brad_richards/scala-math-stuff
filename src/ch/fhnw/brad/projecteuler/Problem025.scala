package ch.fhnw.brad.projecteuler
import java.math.BigInteger
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem025 {

  def main(args: Array[String]): Unit = {
    var minVal : BigInt = 1
    for (i <- 1 to 999) minVal *= 10
    
    val fibonacciNumbers = MathUtils.bigFibonacci(minVal)
    val last = fibonacciNumbers(fibonacciNumbers.length-1)
    val logLast = last.toString().length()
    println(fibonacciNumbers.length)
  }

}