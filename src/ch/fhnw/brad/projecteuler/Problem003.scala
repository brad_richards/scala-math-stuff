package ch.fhnw.brad.projecteuler
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem003 {
  def main(args: Array[String]): Unit = {
    val primes = MathUtils.primeFactors(600851475143l)

    primes foreach (p => println(p))
  }
}