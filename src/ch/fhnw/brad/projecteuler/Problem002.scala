package ch.fhnw.brad.projecteuler
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem002 {

  def main(args: Array[String]): Unit = {
    val fibonacciNumbers = MathUtils.fibonacci(4000000)
    var sum : BigInt = 0
    for (n <- fibonacciNumbers) {
      if (n % 2 == 0) sum += n
    }
    println(sum)
  }

}