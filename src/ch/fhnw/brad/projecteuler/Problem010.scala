package ch.fhnw.brad.projecteuler
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem010 {

  def main(args: Array[String]): Unit = {
    val primes = MathUtils.primes(2000000)
    var sum : Long = 0
    primes foreach (p => sum += p)
    println(sum)
  }

}