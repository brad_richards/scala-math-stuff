package ch.fhnw.brad.projecteuler
import scala.annotation.tailrec

object Problem014 {
  def main(args: Array[String]): Unit = {
    var longest : Long = 0
    var value : Long = 0
    
    for (i <- 2 until 1000000) {
      val steps = collatz(i)
      if (steps > longest) {
        longest = steps
        value = i
      }
    }
    
    println("Value " + value + " requires " + longest + " steps")
  }
  
  /**
   * Return number of steps required to reach 1, following
   * the terms of the Collatz conjecture
   */
  def collatz(n : Long) : Long = {
    @tailrec
    def collatz2(steps : Long, n : Long) : Long = {
      if (n == 1) {
        steps
      } else {
        if (n % 2 == 0) {
          collatz2(steps+1, n/2)
        } else {
          collatz2(steps+1, 3*n+1)
        }
      }
    }
    collatz2(0,n)
  }
}