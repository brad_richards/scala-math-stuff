package ch.fhnw.brad.projecteuler.util
import scala.annotation.tailrec
import scala.collection.mutable.ListBuffer

object MathUtils {
  
  def factorial(n : Int) : Int = {
    @tailrec
    def fact2(n : Int, value : Int) : Int = {
      if (n < 2) {
        value
      } else {
        fact2(n-1, value*n)
      }
    }
    
    fact2(n, 1)
  }
  
    def bigFactorial(n : BigInt) : BigInt = {
    @tailrec
    def fact2(n : BigInt, value : BigInt) : BigInt = {
      if (n < 2) {
        value
      } else {
        fact2(n-1, value*n)
      }
    }
    
    fact2(n, 1)
  }
  
  def fibonacci(max : Int) : List[Int] = {    
    @tailrec
    def fibonacciBuilder(max : Int, numbers : List[Int]) : List[Int] = {
      if (numbers.head >= max) {
        numbers
      } else {
    	var nextElt : Int = numbers(0) + numbers(1)
        fibonacciBuilder(max, nextElt +: numbers)
      }
    }

    fibonacciBuilder(max, List(1,1)).reverse
  }
  
  def bigFibonacci(max : BigInt) : List[BigInt] = {
    @tailrec
    def bigFibonacciBuilder(max : BigInt, numbers : List[BigInt]) : List[BigInt] = {
      if (numbers.head >= max) {
        numbers
      } else {
    	var nextElt : BigInt = numbers(0) + numbers(1)
        bigFibonacciBuilder(max, nextElt +: numbers)
      }
    }

    bigFibonacciBuilder(max, List(1,1)).reverse
  }
  
  /**
   * Return a boolean array corresponding to the integers from 2 through max.
   * For each entry, a value of "true" indicates that the integer is prime.
   */
  def erastothenes(max : Int) : Array[Boolean] = {
    var integers = new Array[Boolean](max-1)
    for (i <- 0 to max-2) integers(i) = true
    
    for (i <- 0 to Math.sqrt(max).toInt - 2) {
      if (integers(i)) { // is a prime
        val prime = i+2
        var index = prime * prime // start at prime-squared
        while (index <= max) {
          integers(index-2) = false
          index += prime
        }        
      }
    }
    integers
  }
  
  /**
   * Return a list of all primes up to (and including) the given maximum value
   */
  def primes(max : Int) : List[Int] = {
    val erastothenesPrimes : Array[Boolean] = erastothenes(max)
    val buf : ListBuffer[Int] = new ListBuffer()
    for (i <- 0 to erastothenesPrimes.length-1) {
      if (erastothenesPrimes(i)) {
        buf += i+2
      }      
    }
    buf.toList
  }
  
  /**
   * Return a set containing the unique prime factors of the given number.
   */
  def primeFactors(number : Long) : Set[Long] = {
    @tailrec
    def primeFactors2(number : Long, primes : List[Int], factors : Set[Long]) : Set[Long] = {
      if (number == 1) { // Factoring finished
        factors
      } else if (primes.isEmpty) { // Tried all factors; number is prime
        factors + number
      } else {
        val nextPrime = primes.head
        if (number % nextPrime != 0) { // This prime is not a factor
          primeFactors2(number, primes.tail, factors)
        } else { // This prime is a factor
          primeFactors2(number / nextPrime, primes, factors + nextPrime)
        }
      }
    }
    
    val primeNumbers = primes(Math.sqrt(number).toInt)
    primeFactors2(number, primeNumbers, Set.empty)    
  }
  
  /**
   * Calculate the integer square root of a BigInt; if the square root is not an
   * integer, we return the largest integer less than the square root (truncation)
   */
  def bigIntSqrt(number : BigInt) : BigInt = {
    val x = bigDecimalSqrt(BigDecimal(number))
    x.toBigInt()
  }
  
  /**
   * Calculate the square-root of a BigDecimal, using Newton's method.
   * See: http://web.archive.org/web/20110714075116/http://www.merriampark.com/bigsqrt.htm
   * Basic steps:
   * 
   * 1. Start with an initial guess (g) of what the square root of the number (n) is.
   * This guess does not have to be even close; you could just take g = 1
   * 
   * 2. Compute ((n/g) + g)/2 and take this as the next guess
   * 
   * 3. Repeat step 2 until the last two results obtained are the same
   * 
   * Note that this method becomes very slow for values above 10^100000
   */
  def bigDecimalSqrt(number : BigDecimal) : BigDecimal = {
    // For small numbers, we just use the built-in function.
    if (number < Int.MaxValue) {
      if (number <= 0) {
        0
      } else {
        Math.sqrt(number.toDouble).toInt
      }
    } else {
      // We take the square root of the leftmost 8 or 9 digits. We then multiply
      // this to add a number of zeroes equal to half the remaining digits. This
      // is then our initial guess. Example: Our initial guess for
      // 12345678900000000 is sqrt(123456789) * 10000
      
      val strVal = number.toString();
      val numDigits = strVal.length()
      val lengthIsEven = (numDigits % 2) == 0
      
      val numLeftDigits = (if (lengthIsEven) 8 else 9)
      val leftDigits = strVal.substring(0, numLeftDigits)
      val numZeroes = (numDigits - numLeftDigits) / 2
      val multiplier = BigDecimal(pow10(numZeroes))
      
      val sqrtLeft = Math.sqrt(leftDigits.toDouble).toInt
      val guess = sqrtLeft * multiplier

      /**
       * Recursive guessing method
       */
      @tailrec
      def bigSqrt2(guess : BigDecimal, number : BigDecimal, iterations : Long) : (BigDecimal, Long) = {
        val newGuess = (number / guess + guess) / 2
        
        if (guess == newGuess) {
          (guess, iterations)
        } else {
          bigSqrt2(newGuess, number, iterations+1)
        }
      }
      val tupleResult = bigSqrt2(guess, number, 0)
      println("Iterations for " + number + " were " + tupleResult._2)
      tupleResult._1
    }    
  }
  
  /**
   * Powers of 10 as BigInts
   */
  def pow10(exponent : Int) : BigInt = {
    var value : BigInt = 1
    for (i <- 1 to exponent ) value *= 10
    value
  }
}