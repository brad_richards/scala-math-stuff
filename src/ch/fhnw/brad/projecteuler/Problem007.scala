package ch.fhnw.brad.projecteuler
import ch.fhnw.brad.projecteuler.util.MathUtils

object Problem007 {

  def main(args: Array[String]): Unit = {
    val primes = MathUtils.primes(1000000)
    println(primes(10000))
  }

}