package ch.fhnw.brad.projecteuler
import scala.collection.mutable.ListBuffer
import scala.io.Source
import javax.swing.JFileChooser
import java.io.File

object Problem013 {

  def main(args: Array[String]): Unit = {
    // Use Java FileChooser to select the file
	val chooser : JFileChooser = new JFileChooser();
	chooser.showOpenDialog(null);		
	val f : File = chooser.getSelectedFile();    

	// Read the file, then close it
  	var buf = new ListBuffer[BigInt]()
  	val file = Source.fromFile(f)
  	for (line <- file.getLines()) {
  	  buf + BigInt(line)
  	}
  	file.close()
  	
  	// Add the numbers
  	var sum : BigInt = 0
  	for (num <- buf) sum += num
  	
  	// String representation, then first 10 chars
  	val digits = sum.toString().substring(0,10)
  	println(digits)
  }
}