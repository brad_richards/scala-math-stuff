package ch.kri.brad.misc

import ch.fhnw.brad.projecteuler.util.MathUtils
import scala.math.BigInt.int2bigInt

object BrownNumbers {

  def main(args: Array[String]): Unit = {
    for (n <- 1 to 100000) {
      val factorialN = MathUtils.bigFactorial(n) + 1
      val sqrtFact = MathUtils.bigIntSqrt(factorialN)
      
      val sqrtSquared = sqrtFact * sqrtFact
      if (sqrtSquared == factorialN) {
        println(sqrtFact + " " + n)
      }
    }    
  }

}